#include "libtty.h"

typedef struct buf_struct {
    char *buf;
    size_t len;
} buf_struct;

// helpers
void clear_test_file(void);
void write_to_test_file(const char* buf);
void refresh_test_fd(void);
int strncmp_test_file(const char* buf, size_t len);

// callbacks int (*serial_routine) (int, void*)
int read_file_callback(int fd_tty, void* buf_arg);
int write_file_callback(int fd_tty, void* buf_arg);

// test callbacks
void test_read_cb(void);
void test_write_cb(void);

// test basic functionality in isolation
void test_read_isol(void);
void test_write_isol(void);
void test_read_NB_isol(void);
void test_write_NB_isol(void);
void test_read_all_isol(void);
void test_write_all_isol(void);
void test_read_all_NB_isol(void);
void test_write_all_NB_isol(void);

// test basic functionality in combination
void test_read_comb(void);
void test_write_comb(void);
void test_read_NB_comb(void);
void test_write_NB_comb(void);
void test_read_all_comb(void);
void test_write_all_comb(void);
void test_read_all_NB_comb(void);
void test_write_all_NB_comb(void);
