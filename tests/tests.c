//=======================================================================================================================
// Written by LAICE software team in Feb, 2020
// Unit Tests for Libtty using CUnit Framework

// Code adapted from:
// http://cunit.sourceforge.net/doc/index.html
//=========================================================================================================================

#include "tests.h"

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "CUnit/Basic.h"
#include "CUnit/Console.h"
#include "CUnit/Automated.h"

#define TEST_FILE "test_file"
static int test_fd = 0;

/* @TODO: break tests into suites via add_tests functions and suites in main */
int add_tests_to_suite(CU_pSuite* pSuite);

/* code to be run in order to initialize the testing suite */
int init_suite_all(void) { 
    if ((test_fd = open(TEST_FILE, O_CREAT | O_TRUNC | O_RDWR, 0744)) == -1) {
        return -1;
    }
    return 0;
}
/* code to be run in order to clean up the testing suite */
int clean_suite_all(void) { 
    if (close(test_fd) != 0) {
        return -1;
    }
    remove(TEST_FILE);
    return 0;
}

int main() {
    CU_pSuite pSuite = NULL;

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    /* add a suite to the registry */
    pSuite = CU_add_suite("Suite_All", init_suite_all, clean_suite_all);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add tests to the suite */
    if (add_tests_to_suite(&pSuite) == -1) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run all tests using the basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_ErrorCode res = CU_basic_run_tests();
    printf("\n");
    CU_basic_show_failures(CU_get_failure_list());
    printf("\n\n");

    return 0;
}

void test_read_cb(void) {
    char* buf= "Hello World\n";
    clear_test_file();
    refresh_test_fd();
    write_to_test_file(buf);

    buf_struct arg = {
        .buf = malloc(strlen(buf) + 1),
        .len = strlen(buf)
    };
    if (test_fd != 0) {
        int res = read_file_callback(test_fd, &arg);
        CU_ASSERT(strlen(buf) == res);
        CU_ASSERT(strncmp(arg.buf, buf, strlen(buf)) == 0);
    }
    free(arg.buf);
}

void test_write_cb(void) {
    clear_test_file();
    refresh_test_fd();

    char* buf= "Hello World\n";
    buf_struct arg = {
        .buf = malloc(strlen(buf) + 1),
        .len = strlen(buf)
    };
    strncpy(arg.buf, buf, arg.len);
    if (test_fd != 0) {
        int res = write_file_callback(test_fd, &arg);
        CU_ASSERT(strlen(buf) == res);
        CU_ASSERT(strncmp_test_file(buf, strlen(buf)) == 0);
    }
    free(arg.buf);
}

// test basic functionality in isolation
void test_read_isol(void) {
    char* buf= "Hello World\n";
    clear_test_file();
    refresh_test_fd();
    write_to_test_file(buf);

    int res;
    buf_struct arg = {
        .buf = malloc(strlen(buf) + 1),
        .len = strlen(buf)
    };
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, read_file_callback, (void *) &arg, &res));
        CU_ASSERT(strlen(buf) == res);
        CU_ASSERT(strncmp(arg.buf, buf, strlen(buf)) == 0);
    }
    free(arg.buf);
}

void test_write_isol(void) {
    clear_test_file();
    refresh_test_fd();

    char* buf= "Hello World\n";
    int res;
    buf_struct arg = {
        .buf = malloc(strlen(buf) + 1),
        .len = strlen(buf)
    };
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, write_file_callback, (void *) &arg, &res));
        CU_ASSERT(strlen(buf) == res);
    }
    free(arg.buf);
}

// test basic functionality in combination
void test_read_comb(void) {
    clear_test_file();
    refresh_test_fd();

    char* buf= "Hello World\n";
    int res;
    buf_struct w_arg = {
        .buf = malloc(strlen(buf) + 1),
        .len = strlen(buf)
    };
    strncpy(w_arg.buf, buf, w_arg.len);
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, write_file_callback, (void *) &w_arg, &res));
        CU_ASSERT(strlen(buf) == res);
    }
    free(w_arg.buf);

    // reset res and refresh fd for read test
    res = 0;
    refresh_test_fd();

    // going to read each half of "Hello World\n" back to back
    buf_struct r_arg = {
        .buf = malloc(6 + 1),
        .len = 6
    };
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, read_file_callback, (void *) &r_arg, &res));
        CU_ASSERT(r_arg.len == res);
        CU_ASSERT(strncmp(r_arg.buf, "Hello ", r_arg.len) == 0);

        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, read_file_callback, (void *) &r_arg, &res));
        CU_ASSERT(r_arg.len == res);
        CU_ASSERT(strncmp(r_arg.buf, "World\n", r_arg.len) == 0);
    }
    free(r_arg.buf);
}

void test_write_comb(void) {
    clear_test_file();
    refresh_test_fd();

    char* buf1= "Hello ";
    char* buf2= "World\n";
    char* buf3= "Hello World\n";
    int res;
    buf_struct w_arg = {
        .buf = malloc(strlen(buf1) + 1),
        .len = strlen(buf1)
    };
    strncpy(w_arg.buf, buf1, w_arg.len);
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, write_file_callback, (void *) &w_arg, &res));
        CU_ASSERT(strlen(buf1) == res);
    }
    strncpy(w_arg.buf, buf2, w_arg.len);
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, write_file_callback, (void *) &w_arg, &res));
        CU_ASSERT(strlen(buf2) == res);
    }
    free(w_arg.buf);

    // reset res and refresh fd for read test
    res = 0;
    refresh_test_fd();

    buf_struct r_arg = {
        .buf = malloc(strlen(buf3) + 1),
        .len = strlen(buf3)
    };
    if (test_fd != 0) {
        CU_ASSERT(EXIT_SUCCESS == serial_callback(test_fd, read_file_callback, (void *) &r_arg, &res));
        CU_ASSERT(strlen(buf3) == res);
        CU_ASSERT(strncmp(r_arg.buf, buf3, strlen(buf3)) == 0);
    }
    free(r_arg.buf);
}

int add_tests_to_suite(CU_pSuite* pSuite) {
    if ((NULL == CU_add_test(*pSuite, "test_read_cb", test_read_cb)) ||
        (NULL == CU_add_test(*pSuite, "test_write_cb", test_write_cb)) ||
        (NULL == CU_add_test(*pSuite, "test_read_isol", test_read_isol)) ||
        (NULL == CU_add_test(*pSuite, "test_write_isol", test_write_isol)) ||
        (NULL == CU_add_test(*pSuite, "test_read_comb", test_read_comb)) ||
        (NULL == CU_add_test(*pSuite, "test_write_comb", test_write_comb)))
    {
       return -1;
    }
    return 0;
}

void refresh_test_fd(void) {
    close(test_fd);
    test_fd = open(TEST_FILE, O_RDWR);
}

void clear_test_file(void) {
    fclose(fopen(TEST_FILE, "w"));
}

void write_to_test_file(const char* buf) {
    FILE* fp = fopen("test_file", "w+"); 
    fprintf(fp, "Hello World\n");
    fclose(fp);
}

int strncmp_test_file(const char* buf, size_t len) {
    char f_buf[len + 1];
    FILE* fp;

    if ((fp = fopen(TEST_FILE, "r")) == NULL) return -1;
    size_t res = fread(f_buf, sizeof(char), len, fp);
    if (ferror(fp) != 0 ) {
        fclose(fp);
        return -1;
    }

    f_buf[res++] = '\0';
    fclose(fp);

    return strncmp(f_buf, buf, len);
}

int read_file_callback(int fd_tty, void* buf_arg) {
    buf_struct* buf_s = (buf_struct *) buf_arg;
    ssize_t res = read(fd_tty, buf_s->buf, buf_s->len);
    return res;
}

int write_file_callback(int fd_tty, void* buf_arg) {
    buf_struct* buf_s = (buf_struct *) buf_arg;
    ssize_t res = write(fd_tty, buf_s->buf, buf_s->len);
    return res;
}
