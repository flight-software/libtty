#ifndef _LIB_TTY_H_
#define _LIB_TTY_H_

#include <stdint.h>
#include <stdlib.h>
#include <termios.h>

struct serial_arg_fs {
    const uint8_t* write_buf;
    size_t write_len;
    uint8_t* read_buf;
    size_t* read_len;
};

typedef struct serial_arg_fs serial_arg_fs;

int start_serial(int* fd, const char* path, speed_t baudrate, unsigned char vmin, unsigned char vtime);
int rs485_setup(int fd);
void close_serial(int fd_tty);
int serial_callback(int fd_tty, int (*serial_routine) (int, const serial_arg_fs* arg), const serial_arg_fs* arg, int* res);

/**
 * If this fails, remember to check EWOULDBLOCK
 */
int serial_callback_NB(int fd_tty, int (*serial_routine) (int, const serial_arg_fs* arg), const serial_arg_fs* arg, int* res);

int acquire_serial_lock(int fd_tty);
int acquire_serial_lock_NB(int fd_tty);
int release_serial_lock(int fd_tty);

int is_standard_baud_rate(speed_t baud_rate);

#endif
