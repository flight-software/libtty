//=======================================================================================================================
// Written by LAICE software team in Oct, 2013
// Shared library for serial-port related functions

// Code adapted from:
// http://www.adventuresinsilicon.com/node/19
// Detail serial port setting in:
// http://www.cmrr.umn.edu/~strupp/serial.html
//=========================================================================================================================

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include "libdebug.h"
#include "libtty.h"

#ifndef _UAPI_LINUX_SERIAL_H
#include <linux/types.h>
#include <asm-generic/ioctls.h>
#endif /* _UAPI_LINUX_SERIAL_H */

#ifndef serial_rs485
struct serial_rs485 {
    __u32   flags;                                      /* RS485 feature flags */
    #define SER_RS485_ENABLED               (1 << 0)    /* If enabled */
    #define SER_RS485_RTS_ON_SEND           (1 << 1)    /* Logical level for RTS pin whensending */
    #define SER_RS485_RTS_AFTER_SEND        (1 << 2)    /* Logical level for RTS pin after sent*/
    #define SER_RS485_RTS_BEFORE_SEND (1 << 3)
    #define SER_RS485_USE_GPIO (1 << 5)
    __u32   delay_rts_before_send;                      /* Delay before send (milliseconds) */
    __u32   delay_rts_after_send;                       /* Delay after send (milliseconds) */
    __u32 gpio_pin;                                     /* GPIO Pin Index */
    __u32 padding[4];
};
#endif 

#define GPIO0_28 (32*0+28)

static int setup_serial(int* fd_tty, const char* path)
{
    if(fd_tty == NULL) return EXIT_FAILURE;

    //open serial path
    *fd_tty = open(path, O_RDWR | O_NOCTTY | O_SYNC);

    if (*fd_tty == -1) {
        P_ERR("Error %d opening %s (%s)\n", errno, path, strerror(errno));
        return EXIT_FAILURE;
    }

    P_INFO("Serial connection established on %s\n",path);
    tcflush(*fd_tty, TCIOFLUSH);
    return EXIT_SUCCESS;
}

//=========================================================================================================================

static int init_termios(int fd_tty, speed_t baud_rate, unsigned char vmin, unsigned char vtime)
{
    struct termios options;
    if(tcgetattr(fd_tty, &options) != 0) {
        P_ERR("Error %d from tcgetattr in %s (%s)\n", errno, strerror(errno), __func__);
        return EXIT_FAILURE;
    }

    // Set the baud rates to whatever is needed... (Max 115200)
    cfsetispeed(&options, baud_rate);
    cfsetospeed(&options, baud_rate);

    options.c_cflag &= ~(CSIZE | PARENB | CSTOPB | CRTSCTS);
    options.c_cflag |= (CLOCAL | CREAD | CS8);

    options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

    options.c_oflag &= ~(OPOST);

    options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    options.c_cc[VMIN]  = vmin;                // vmin input bytes are enough to return from read()
    options.c_cc[VTIME]  = vtime;              // Or wait 0.1*vtime s if nothing to read

    if(tcsetattr(fd_tty, TCSANOW, &options) != 0) {
        P_ERR("Error %d from tcsetattr in %s (%s)\n", errno, strerror(errno), __func__);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//=========================================================================================================================

static int init_termios2(int fd_tty, speed_t baud_rate, unsigned char vmin, unsigned char vtime)
{
    #include <asm/termios.h>
    // web.archive.org/web/20160421013630/https://www.downtowndougbrown.com/2013/11/linux-custom-serial-baud-rates/
    // use termios2 for custom baud rates
    struct termios2 options;
    if(ioctl(fd_tty, TCGETS2, &options) != 0) {
        P_ERR("Error TCGETS2 %d, %s", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    options.c_cflag &= ~(CSIZE | PARENB | CSTOPB | CRTSCTS);
    options.c_cflag |= (CLOCAL | CREAD | CS8);

    options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

    options.c_oflag &= ~(OPOST);

    options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    options.c_cc[VMIN]  = vmin;                // vmin input bytes are enough to return from read()
    options.c_cc[VTIME]  = vtime;              // Or wait 0.1*vtime s if nothing to read

    // non standard baud rate specifc
    options.c_cflag &= ~CBAUD;
    options.c_cflag |= BOTHER;

    options.c_ispeed = baud_rate;

    options.c_ospeed = baud_rate;

    if(ioctl(fd_tty, TCSETS2, &options) != 0) {
        P_ERR("Error TCSETS2 %d, %s", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//=========================================================================================================================

static int init_serial(int fd_tty, speed_t baud_rate, unsigned char vmin, unsigned char vtime)
{
    if (is_standard_baud_rate(baud_rate)) {
        return init_termios(fd_tty, baud_rate, vmin, vtime);
    } else {
        return init_termios2(fd_tty, baud_rate, vmin, vtime);
    }
    return EXIT_SUCCESS;
}

//=========================================================================================================================

int start_serial(int* fd, const char* path, speed_t baudrate, unsigned char vmin, unsigned char vtime)
{
    if (setup_serial(fd, path)) {
        P_ERR_STR("Could not setup serial port - exiting\n");
        return EXIT_FAILURE;
    }

    if (init_serial(*fd, baudrate, vmin, vtime)) {
        P_ERR_STR("Could not init serial port - exiting\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//=========================================================================================================================

int rs485_setup(int fd)
{
    struct serial_rs485 rs485conf = {0};

    rs485conf.flags = SER_RS485_ENABLED;
    rs485conf.flags |= SER_RS485_USE_GPIO;
    rs485conf.flags |= SER_RS485_RTS_ON_SEND;
    rs485conf.gpio_pin = GPIO0_28;
    rs485conf.delay_rts_before_send = 1;
    rs485conf.delay_rts_after_send = 1;
    int status = ioctl(fd, TIOCSRS485, &rs485conf);
    if(status < 0) {
        P_ERR("Unable to set IOCTL. IOCTL return value: %d, error: %s\n", status, strerror(errno));
        return EXIT_FAILURE;
    }   
    return EXIT_SUCCESS;
}

//=========================================================================================================================

void close_serial(int fd_tty)
{
    // Clean up the connection:
    tcflush(fd_tty, TCIOFLUSH); // Flushes data received but not read, and data written but not transmitted
    close(fd_tty);
}

//=========================================================================================================================

int serial_callback(int fd_tty, int (*serial_routine) (int, const serial_arg_fs* arg), const serial_arg_fs* arg, int* res)
{
    if (acquire_serial_lock(fd_tty) == -1) return EXIT_FAILURE;  

    if (res == NULL) serial_routine(fd_tty, arg);
    else *res = serial_routine(fd_tty, arg);

    if (release_serial_lock(fd_tty) == -1) return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

//=========================================================================================================================

int serial_callback_NB(int fd_tty, int (*serial_routine) (int, const serial_arg_fs* arg), const serial_arg_fs* arg, int* res)
{
    if (acquire_serial_lock_NB(fd_tty) == -1) return EXIT_FAILURE;  

    if (res == NULL) serial_routine(fd_tty, arg);
    else *res = serial_routine(fd_tty, arg);

    if (release_serial_lock(fd_tty) == -1) return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

//=========================================================================================================================

int acquire_serial_lock(int fd_tty)
{
    return flock(fd_tty, LOCK_EX);
}

//=========================================================================================================================

int acquire_serial_lock_NB(int fd_tty)
{
    return flock(fd_tty, LOCK_EX | LOCK_NB);
}

//=========================================================================================================================

int release_serial_lock(int fd_tty)
{
    return flock(fd_tty, LOCK_UN);
}

//=========================================================================================================================

int is_standard_baud_rate(speed_t baud_rate)
{
    return baud_rate == B50 ||
       baud_rate == B75     ||
       baud_rate == B110    ||
       baud_rate == B134    ||
       baud_rate == B150    ||
       baud_rate == B200    ||
       baud_rate == B300    ||
       baud_rate == B600    ||
       baud_rate == B1200   ||
       baud_rate == B1800   ||
       baud_rate == B2400   ||
       baud_rate == B4800   ||
       baud_rate == B9600   ||
       baud_rate == B19200  ||
       baud_rate == B38400  ||
       baud_rate == B57600  ||
       baud_rate == B115200;
}
